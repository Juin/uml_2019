# UML 2019/11/05

* https://www.omg.org/spec/UML/2.5.1/PDF
* https://gitlab.com/Juin/uml_2019
* Gestion d'une bibliotéque:
    https://www.irit.fr/~Veronique.Gaildrat/Ressources/EnsAnnales/Bibliotheque.pdf

## Extrait de REAC REAC_CDA_V03_03052018.pdf:

* Traduire les besoins en diagrammes UML
* Connaissance cas d'utilisation
* Concevoir la solution à partir des diagrammes UML
* Utiliser les fonctionnalités de génération de code de l’outil de modélisation UML
* Connaissance des diagrammes UML concernant les composants  et le déploiement
* Connaissance diagramme d'état
* Connaissance diagramme d’activité 

## Jour 1

### Cours:
http://info.arqendra.net/Files/_UML_cours.pdf

### Plan:

* qu'est ce que UML
* use case
* diagramme de séquence
* diagramme de classe

### TD:
Réaliser l'étude UML d'un outil de gestion de Médiathèque:

* Use case
* Sequence
* Class
* Sequence "détailé"


## Jour 2

### Cours:
http://laurent-audibert.developpez.com/Cours-UML/uml2-apprentissage-pratique.pdf

### Plan

* Diagramme d'activité
* Diagramme d'état transition

### TD:
Réaliser le diagramme d'activité d'un boutique en ligne/d'une bibliotèque


## Jour 3

### Plan

* Deploiment
* Composant