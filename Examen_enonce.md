## MISE EN SITUATION

Le consortium de compagnies aériennes Blue Sky veut un logiciel pour :

* gérer les vols et les avions des compagnies
* gérer  les  achats  de  billets  et  l’enregistrement  des  passagers.
  Ces opérations  peuvent  être  faites  en  ligne  par  les  clients,  ou  à  un  guichet tenu par une hôtesse.
  L’enregistrement peut aussi être fait par le client à un guichet automatique dans un aéroport.
* générer des tableaux de données pour les responsables clientèle des compagnies  et  pour  les  services  de  sécurité,
  en  particulier  la  liste  des passagers d’un vol.

Les précisions suivantes sont données par Blue Sky :

* un vol est ouvert et fermé à l’achat de billet et à l’enregistrement sur ordre de la compagnie.
* un  client  peut  acheter  un  ou  plusieurs  billets,  pour  des  passagers différents mais il doit fournir un numéro de passeport pour chaque billet.
* un billet concerne un seul passager.
* l’enregistrement peut donner lieu au paiement d’un supplément bagage.
* un billet peut être annulé tant que le vol n’a pas eu lieu.
* un vol a un aéroport de départ et un aéroport d’arrivée ainsi qu’un jour et une heure de départ et d’arrivée.
* un billet peut comporter plusieurs vols avec des escales.

Remarque : on néglige l’identification des acteurs dans le système

## DEMANDE

* Réaliser l'étude fonctionelle à l'aide de diagramme USE CASE.
  Il est possible de faire plusieurs diagrammes USE CASE pour isoler des ensembles de foncionalités et améliorer la lisibilité.

* Réaliser les diagramme de séquence pour les scénaorio:
   1. Un employé de la companie achete un billet pour un client
   2. Un employé enregistre un client sur un vol

* Réaliser un diagramme d'état transition pour les vols.

* Réaliser un diagramme d'activité illustrant l'achat du billet jusqu'a l'enregistrement (achat par le client et achat par l'employé du billet)

* Réaliser un diagramme de class à partir des elments précédent.

* Imaginer un diagramme de déploiement pour cette application.
